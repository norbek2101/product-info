from rest_framework import generics, views

from jadval.models import ProductMaterials, Warehouse
from mahsulot.models import Product
from xomashyo.models import Material

from jadval.serializers import MainSerializer, ProductMaterialsSerializer, WarehouseSerializer

class ProductMaterialsListCreateView(generics.ListCreateAPIView):
    """
        Mahsulot uchun kerak bo'ladigan xomashyolar ro'yxatini ko'rish va yangi productmaterial qo'shish uchun API
    """
    queryset = ProductMaterials.objects.all()
    serializer_class = ProductMaterialsSerializer


class ProductMaterialsDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
        Mahsulot uchun kerak bo'ladigan xomashyolarni ko'rish, o'zgartirish va o'chirish uchun API
    """
    queryset = ProductMaterials.objects.all()
    serializer_class = ProductMaterialsSerializer


class AllProductMaterialsView(views.APIView):
    """
        Mahsulot uchun kerak bo'ladigan barcha xomashyolarini ko'rish uchun API
    """
    queryset = ProductMaterials.objects.all()
    serializer_class = ProductMaterialsSerializer
    
    def get(self, request, *args, **kwargs):
        """
        Mahsulot uchun kerak bo'ladigan barcha xomashyolarni ko'rish uchun get method
        """
        id = self.kwargs.get('pk')
        try:
            product = Product.objects.get(id=id)
            queryset = ProductMaterials.objects.filter(product=product.id)
            serializer = ProductMaterialsSerializer(queryset, many=True)
            return views.Response(serializer.data)
        
        except Product.DoesNotExist:
            return views.Response({"Detail": "Query doesn't match any Product object with given id"}, status=404)
        
    
class PartiyaListCreateView(generics.ListCreateAPIView):
    """
        Partiyalar ro'yxatini ko'rish va yangi partiya qo'shish uchun API
    """
    queryset = Warehouse.objects.all()
    serializer_class = WarehouseSerializer


class PartiyaDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
        Partiyalar ro'yxatini ko'rish, o'zgartirish va o'chirish uchun API
    """
    queryset = Warehouse.objects.all()
    serializer_class = WarehouseSerializer


class AllPartiyaView(views.APIView):
    """
        Bir xomashyo nechta partiyalardan iborat ekanligini ko'rish uchun API
    """
    queryset = ProductMaterials.objects.all()
    serializer_class = MainSerializer
    
    def get(self, request, *args, **kwargs):
        """
            Bir xomashyo nechta partiyalardan iborat ekanligini ko'rish uchun get method
        """
        id = self.kwargs.get('pk')
        try:
            material = Material.objects.get(id=id)
            queryset = Warehouse.objects.filter(material=material.id)
            serializer = WarehouseSerializer(queryset, many=True)
            return views.Response(serializer.data)
        
        except Warehouse.DoesNotExist:
            return views.Response({"Detail": "Query doesn't match any Warehouse object with given id"}, status=404)


class MainView(views.APIView): 
    """
        Mahsulot uchun kerak bo'ladigan xomashyolarni va ularning partiyalarini ko'rish uchun API
    """
    def post(self, request, *args, **kwargs):
        products_info = request.data 
        result = []
        material_deductions = {}

        for product_info in products_info:
            try:
                product = Product.objects.get(name=product_info['product_name'])
                materials = self.calculate_materials(product, product_info['product_qty'], material_deductions)

                result.append({
                    'product_name': product.name, 
                    'product_qty': product_info['product_qty'], 
                    'product_materials': materials,
                })

            except Product.DoesNotExist:
                return views.Response({"error": f"{product_info['product_name']} nomli mahsulot topilmadi"}, status=404)
            
        return views.Response({"result": result})


    def calculate_materials(self, product, product_qty, material_deductions):
        """
            Mahsulot uchun kerak bo'ladigan xomashyolar miqdorini  hisoblash uchun funksiya
        """
        product_materials = ProductMaterials.objects.filter(product=product)
        materials_list = []

        for product_material in product_materials:
            required_qty = product_material.quantity * product_qty
            warehouses = Warehouse.objects.filter(material=product_material.material).order_by('price')
            
            for warehouse in warehouses:
                if required_qty <= 0:
                    break
                
                already_deducted = material_deductions.get(warehouse.id, 0)
                effective_reminder = warehouse.reminder - already_deducted

                if effective_reminder > 0:
                    take_qty = min(effective_reminder, required_qty)
                    materials_list.append({
                        "warehouse_id": warehouse.id,
                        "material_name": warehouse.material.name,
                        "qty": take_qty,
                        "price": warehouse.price
                    })
                    required_qty -= take_qty
                    material_deductions[warehouse.id] = already_deducted + take_qty

            if required_qty > 0:  
                materials_list.append({
                    "warehouse_id": None,
                    "material_name": product_material.material.name,
                    "qty": required_qty,
                    "price": None  
                })

        return materials_list
