from django.db import models
from mahsulot.models import BaseModel

class ProductMaterials(BaseModel):
    product = models.ForeignKey('mahsulot.Product', on_delete=models.CASCADE)
    material = models.ForeignKey('xomashyo.Material', on_delete=models.CASCADE)
    quantity = models.FloatField()

    def __str__(self):
        return f"{self.product} - {self.material}"

    class Meta:
        verbose_name = 'ProductMaterial'
        verbose_name_plural = 'ProductMaterial'


class Warehouse(BaseModel):
    material = models.ForeignKey('xomashyo.Material', on_delete=models.CASCADE)
    reminder = models.FloatField()
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f"{self.material} - {self.reminder}"

    class Meta:
        verbose_name = 'Warehouse'
        verbose_name_plural = 'Warehouses'