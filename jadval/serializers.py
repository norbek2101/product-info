from rest_framework import serializers
from jadval.models import ProductMaterials, Warehouse
from mahsulot.serializers import ProductSerializer


class ProductMaterialsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductMaterials
        fields = ('id', 'product', 'material', 'quantity')
        read_only_fields = ('created_at', 'updated_at')

    
    def to_representation(self, instance):
        representation = dict()
        representation['id'] = instance.id
        representation['product'] = instance.product.name
        representation['material'] = instance.material.name
        representation['quantity'] = instance.quantity
        return representation
    

class WarehouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Warehouse
        fields = ('id', 'material', 'reminder', 'price')
        read_only_fields = ('created_at', 'updated_at')
    
    def to_representation(self, instance):
        representation = dict()
        representation['id'] = instance.id
        representation['material'] = instance.material.material.name
        representation['reminder'] = instance.reminder
        representation['price'] = instance.price
        return representation
    

class MwareHouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Warehouse
        fields = ('id', 'material', 'reminder', 'price')
        read_only_fields = ('created_at', 'updated_at')
    
    def to_representation(self, instance):
        representation = dict()
        representation['warehouse_id'] = instance.id
        representation['material_name'] = instance.material.material.name
        representation['qty'] = instance.reminder
        representation['price'] = instance.price
        return representation


class MainSerializer(serializers.Serializer):
    warehouse = MwareHouseSerializer(many=True)
    product_qty = serializers.FloatField()
    
    def to_representation(self, instance):
        representation = super(ProductSerializer, self).to_representation(instance)
        representation['product'] = instance.product.name
        representation['product_qty'] = instance.product_qty
        representation['warehouse'] = instance.warehouse
        return representation


class WarehouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Warehouse
        fields = ('id', 'material', 'reminder', 'price')


# class ProductSerializer(serializers.Serializer):
#     product_name = serializers.CharField(source='name')
#     product_qty = serializers.IntegerField()
    