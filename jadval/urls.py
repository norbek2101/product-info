from django.urls import path
from jadval.views import AllPartiyaView, AllProductMaterialsView, ProductMaterialsListCreateView, ProductMaterialsDetailView, PartiyaDetailView, PartiyaListCreateView, \
    MainView

urlpatterns = [
    #Url patterns to perform CRUD operations for ProductMaterials
    path('pmaterials/', ProductMaterialsListCreateView.as_view(), name='productmaterials-list-create'), 
    path('pmaterial/<int:pk>/', ProductMaterialsDetailView.as_view(), name='productmaterials-detail'),

    #Bir mahsulot uchun kerak bo'ladigan barcha xomashyolarni ko'rish uchun url pattern
    path('ppmaterials/<int:pk>/', AllProductMaterialsView.as_view(), name='all-productmaterials'),

    #Url patterns to perform CRUD operations for Warehouse
    path('partiya/', PartiyaListCreateView.as_view(), name='partiya-list-create'),
    path('partiya/<int:pk>/', PartiyaDetailView.as_view(), name='partiya-detail'),

    path('mpartiya/<int:pk>/', AllPartiyaView.as_view(), name='m-partiya'),

     path('main/', MainView.as_view(), name='main-view'),
]