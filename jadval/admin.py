from django.contrib import admin
from jadval.models import ProductMaterials, Warehouse

admin.site.register(ProductMaterials)
admin.site.register(Warehouse)