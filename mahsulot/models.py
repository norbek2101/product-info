from django.db import models


class BaseModel(models.Model):
    created_at = models.DateTimeField('Created date', auto_now_add=True)
    updated_at = models.DateTimeField('Updated date', auto_now=True)

    class Meta:
        abstract = True


class Product(BaseModel):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'