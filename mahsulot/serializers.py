from rest_framework import serializers
from mahsulot.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'code', 'created_at', 'updated_at')
        read_only_fields = ('created_at', 'updated_at')

    
    def to_representation(self, instance):
        # representation = super(ProductSerializer,self).to_representation(instance)
        representation = dict()
        representation['id'] = instance.id
        representation['product_name'] = instance.name  
        representation['product_code'] = instance.code                  

        return representation