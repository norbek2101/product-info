from django.urls import path
from mahsulot.views import ProductListCreateView, ProductDetailView


urlpatterns = [
    path('products/', ProductListCreateView.as_view(), name='product-list-create'), #<-- Bu url pattern ProductListCreateView uchun
    path('product/<int:pk>/', ProductDetailView.as_view(), name='product-detail'),  #<--Bu url pattern ProductDetailView uchun
]