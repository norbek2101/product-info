from rest_framework import generics

from mahsulot.models import Product
from mahsulot.serializers import ProductSerializer


class ProductListCreateView(generics.ListCreateAPIView):
    """
        ListCreateView: Mahsulotlar ro'yxatini ko'rish va yangi mahsulot qo'shish uchun API
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

   
class ProductDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
        Mahsulotni ko'rish, o'zgartirish va o'chirish uchun API
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer