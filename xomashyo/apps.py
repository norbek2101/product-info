from django.apps import AppConfig


class XomashyoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'xomashyo'
