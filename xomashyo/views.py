from rest_framework import generics

from xomashyo.models import Material
from xomashyo.serializers import MaterialSerializer


class MaterialListCreateView(generics.ListCreateAPIView):
    """
         Xomashyolar ro'yxatini ko'rish va yangi mahsulot qo'shish uchun API
    """
    queryset = Material.objects.all()
    serializer_class = MaterialSerializer

   
class MaterialDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
        Xomashyoni ko'rish, o'zgartirish va o'chirish uchun API
    """
    queryset = Material.objects.all()
    serializer_class = MaterialSerializer
