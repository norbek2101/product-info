from django.urls import path
from xomashyo.views import MaterialListCreateView, MaterialDetailView

urlpatterns = [
    path('materials/', MaterialListCreateView.as_view(), name='material-list-create'), #<-- Bu url pattern MaterialListCreateView uchun
    path('material/<int:pk>/', MaterialDetailView.as_view(), name='material-detail'), #<--Bu url pattern MaterialDetailView uchun
]