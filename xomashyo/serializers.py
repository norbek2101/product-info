from rest_framework import serializers
from xomashyo.models import Material


class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Material
        fields = ('id', 'name')
        read_only_fields = ('created_at', 'updated_at')
    

    def to_representation(self, instance):
        representation = dict()
        representation['id'] = instance.id
        representation['material_name'] = instance.name
        return representation