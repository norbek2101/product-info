# Product Info

A project for getting info about materials in warehouse that required for product


## Prerequisites

Before you begin, ensure you have met the following requirements:
- Python 3.x installed
- PostgreSQL installed
- Virtual environment tool (e.g., venv or virtualenv)

## Installation

A step-by-step guide on how to get a development environment running.

1. Clone the repository:
```bash
git clone https://gitlab.com/norbek2101/product-info.git
cd product-info
```

2. Set up a virtual environment:
```bash
python3 -m venv venv
# Activate the virtual environment
# On Windows
venv\Scripts\activate
# On Unix or MacOS
source venv/bin/activate
```

3. Install the dependencies:
```bash
pip install -r requirements.txt
```

4. Set up your `.env` file as per the template provided in the project (make sure to adjust the values accordingly):
```
# Django settings
DEBUG=True
SECRET_KEY=your_django_secret_key_here
ALLOWED_HOSTS=localhost,127.0.0.1

# Database configuration for PostgreSQL
DATABASE_NAME=your_database_name
DATABASE_USER=your_database_user
DATABASE_PASSWORD=your_database_password
DATABASE_HOST=localhost
DATABASE_PORT=5432
```

5. Run migrations to create the database schema:
```bash
python manage.py migrate
```

6. Start the development server:
```bash
python manage.py runserver
```

Now, navigate to `http://127.0.0.1:8000` in your browser to see the application running.

## Usage


**Main api for getting info about materials in warehouse that reqired for product**

*Method: POST*

> URL: localhost:8000/api/v1/main/

```
REQUEST DATA:
        [
            {
                "product_name": "Koylak",
                "product_qty": 30
            },
            {
                "product_name": "Shim",
                "product_qty": 20
            }
        ]

```

**Api for creating product**

*Method: POST*

> URL: localhost:8000/api/v1/products/

```
REQUEST DATA:
            {
                "name": "Kostyum",
                "code": K21
            }

```

**Api for viewing all products**

*Method: GET*

> URL: localhost:8000/api/v1/products/
```





```


**Api for creating material**

*Method: POST*

> URL: localhost:8000/api/v1/materials/


```
REQUEST DATA:
            {
                "name": "Mato"
            }

```

**Api for viewing all materials**

*Method: GET*

> URL: localhost:8000/api/v1/materials/
```





```


**Api for creating product-material**

*Method: POST*

> URL: localhost:8000/api/v1/pmaterials/


```
REQUEST DATA:
            {
                "product": 1,
                "material": 1,
                "quantity": 5
            }
```

**Api for viewing all product-materials**

*Method: GET*

> URL: localhost:8000/api/v1/pmaterials/
```




```


**Api for viewing all product-materials by product ID**

*Method: GET*

> URL: localhost:8000/api/v1/ppmaterials/1/
```




```


**Api for creating warehouse(partiya)**

*Method: POST*

> URL: localhost:8000/api/v1/partiya/


```
REQUEST DATA:
            {
                "material": 1,
                "reminder": 40,
                "price": 1500
            }
```

**Api for viewing all warehouses(partiya)**

*Method: GET*

> URL: localhost:8000/api/v1/partiya/
```




```


**Api for viewing all warehouse by material ID**

*Method: GET*

> URL: localhost:8000/api/v1/mpartiya/1/
```




```